# README #

This readme is a general overview of the Social Network Node Server for COMP 3715.

### Quick Info ###

Authors:  
Devin Marsh  
Stephen Pelly    
Jaimee Bessey   
Donald Ryan  

## Contents ##

1. Getting Started
  * Setup
  * Getting The Project

## 1. Getting Started ##

### Setup ###

* Create yourself a bitbucket account 
* Download [Git](https://git-scm.com/)
* (Optional) Download [SourceTree](https://www.sourcetreeapp.com/) (this makes life easier)
* Check out the [documentation](https://git-scm.com/doc) for Git. Specifically look at commits, branches, pull/push, and merges.

### Getting The Project ###

The following walkthrough assumes you are using Eclipse and SourceTree, as well as the Node js plugin for Eclipse.

* Create a new empty folder
* In SourceTree, click on Clone/New. Enter "https://dev909@bitbucket.org/dev909/node_server.git" for the Source Path. Select the new folder that you created for the Destination Path (this will download the Git repository to your computer).
* In Eclipse, create a new Node Express project.
* Once the project is created, copy the contents from the folder you cloned the repo from, to the project's directory. Refresh your Eclipse workspace.
* The Git repo should now be in your project, with all the code available.