// Requirements
var express = require('express')
var path = require('path')

// Actual Express object
var app = express()

// Various variables
var port = 3335;

// Request handlers
var indexHandler = require('./routes/index.js');

// Handler functions
app.use('/', indexHandler);

app.listen(port, function () {
  console.log('Example app listening on port ' + port)
})