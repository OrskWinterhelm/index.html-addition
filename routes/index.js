var express = require('express');
var router = express.Router();
var path = require('path')

// index.html path
var indexPage = "/../public/pages/index.html";

/* GET home page. */
router.get('/', function (req, res) {
	// Return the index.html page
    res.sendFile(path.join(__dirname+indexPage));
})

module.exports = router;
